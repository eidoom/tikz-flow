TEX=pdflatex
SRC=$(wildcard *.tex)
BAS=$(basename $(SRC))
PDF=$(addsuffix .pdf, $(BAS))

.PHONY: all clean

all: $(PDF)

clean:
	-rm *.aux *.log *.pdf

$(PDF): %.pdf: %.tex
	$(TEX) $<
