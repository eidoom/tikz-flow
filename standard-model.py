#!/usr/bin/env python3

import math


def radius(area):
    return math.sqrt(area / math.pi)


def diameter(area):
    return 2 * radius(area)


if __name__ == "__main__":
    # data from PDG 2021
    m = {}  # MeV

    # charged leptons
    m["e"] = 0.5109989461
    m["mu"] = 105.6583745
    m["tau"] = 1776.86

    # quarks
    m["u"] = 2.16
    m["d"] = 4.67
    m["c"] = 1270
    m["s"] = 93
    m["t"] = 172760
    m["b"] = 4180

    # bosons
    m["W"] = 80379
    m["Z"] = 91187.6
    m["H"] = 125250

    m = {k: v for k, v in m.items()}

    ma = {n: diameter(math.pow(a, 1 / 2)) / 2 for n, a in m.items()}

    for n, a in ma.items():
        print(f"\\newcommand{{\\mm{n}}}{{{a}}}")
